# Description

Use the linux network namespaces to start applications with individual
interface, connected over a bridge. Therefore, the veth interface is added to
the given bridge with the appropriated address.


    Usage: $(basename $0) [Options] -- exec with args

    -n   Name of the network namespace
         (default: ns)
    -b   Name of the bridge to use
         (default: golden_gate)
    -a   The interface address (with submask, e.g. 1.2.3.4/24)
         (default: 10.0.5.1/24)

# Examples

*   `./netns -n shell -a 10.0.5.1/24 -- bash`

*   First shell: 
    ~~~bash
    ./netns -n listen -a 10.0.33.33/24 -b netcat -- \
      ncat -l 3333
    ~~~
    Second shell:
    ~~~bash
    ./netns -n send -a 10.0.33.34/24 -b netcat -- \
      ncat 10.0.33.33 3333
    ~~~
